let collection = [];

// Write the queue functions below.

// -----------> ENQUEUE <-----------
function print(){

	return collection
}


// -----------> ENQUEUE <-----------
function enqueue(name){

	collection[collection.length] = name

	return collection;
}


// -----------> DEQUEUE <-----------
function dequeue(){

	collection.shift()

	return collection;
}


// -----------> GET FIRST ELEMENT <-----------
function front(){

	return collection[0];
}


// -----------> GET QUEUE SIZE <-----------
function size(){

	return collection.length;
}

// -----------> CHECK IF QUEUE IS NOT EMPTY <-----------
function isEmpty(){

	if(collection.length === 0){
		return true

	} else {
		return false
	}
}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};

